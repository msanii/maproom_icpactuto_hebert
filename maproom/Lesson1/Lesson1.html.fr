<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
xmlns:wms="http://www.opengis.net/wms#"
xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
xmlns:xs="http://www.w3.org/2001/XMLSchema#"
version="XHTML+RDFa 1.0"
>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
  <link class="share" rel="canonical" href="Lesson1.html" />
  <link class="altLanguage" rel="alternate" hreflang="en" href="Lesson1.html?Set-Language=en" />
  <link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
  
  <script type="text/javascript" src="/uicore/uicore.js"></script>

  <style>  

  </style>
  
</head>

<body xml:lang="fr">
  <form name="pageform" id="pageform">

  </form>
  
  <div class="controlBar">

  </div>
  
  <div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Lesson 1 tab</a></li>
      <li><a href="#tabs-2" >Lesson 1.1</a></li>
      <li><a href="#tabs-3" >Lesson 1.2</a></li>
      <li><a href="#tabs-4" >Lesson 1.3</a></li>
    </ul>

    <div id="tabs-1" class="ui-tabs-panel" about="">
      <h2>Lesson 1</h2>
      <p>This tutorial will teach you the main features that can be set up in a Maproom.
      </p>
    </div>

    <div id="tabs-2" class="ui-tabs-panel">
      <h2>Lesson 1.1</h2>
  </div>

  <div id="tabs-3" class="ui-tabs-panel">
    <h2>Lesson 1.2</h2>
  </div>

  <div id="tabs-4"  class="ui-tabs-panel">
    <h2>Lesson 1.3</h2>
  </div>
</div>

<br/>

<div class="optionsBar">
  <fieldset class="navitem" id="share"><legend>Share</legend></fieldset>
</div>

</body>
</html>

